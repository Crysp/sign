var webpack     = require('webpack');
var path        = require('path');
var env         = require('minimist')(process.argv.slice(2));

var IS_DEV          = !env.production;
var IS_PRODUCTION   = !IS_DEV;

module.exports = {

    entry: (function () {
        return './src/' + env.task + '/js/index.js';
    })(),

    output: (function () {
        return {
            path: 'build/' + env.task + '/js',
            filename: env.task + '.bundle.js',
            chunkFilename: 'entry.[id].' + env.task + '.js'
        };
    })(),

    plugins: (function () {

        var plugins = [];

        plugins.push(new webpack.optimize.DedupePlugin());

        if (IS_PRODUCTION) {
            plugins.push(new webpack.DefinePlugin({
                "process.env": {
                    NODE_ENV: JSON.stringify("production")
                }
            }));
            plugins.push(new webpack.optimize.UglifyJsPlugin());
        }

        return plugins;
    })(),

    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: { presets: ['react', 'es2015', 'stage-0'] }
            },
            {
                test: /\.scss$/i,
                loaders: ['style', 'css?modules&camelCase&localIdentName=[name]__[local]', 'sass']
            }
        ]
    },

    resolve: {
        root: path.join(__dirname, 'node_modules'),
        alias: {}
    }

};