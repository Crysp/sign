var gulp = require('gulp');
var util = require('gulp-util');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var env = require('minimist')(process.argv.slice(2));

gulp.task('css', function () {
    return gulp.src('src/' + env.p + '/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(gulp.dest('build/' + env.p + '/css'));
});

gulp.task('css:w', function () {
    gulp.watch('src/' + env.p + '/sass/**/*.scss', ['css']);
});

gulp.task('html', function () {
    return gulp.src('src/' + env.p + '/index.html')
        .pipe(replace('/build/' + env.p + '/', ''))
        .pipe(gulp.dest('build/' + env.p));
});

gulp.task('assets', function () {
    return gulp.src('src/' + env.p + '/fonts/**/*.*')
        .pipe(gulp.dest('build/' + env.p + '/fonts'));
});

gulp.task('build', ['css', 'html', 'assets'], function (done) {
    return done();
});

gulp.task('watch', function () {
    gulp.watch('src/' + env.p + '/index.html', ['html']);
    gulp.watch('src/' + env.p + '/sass/**/*.scss', ['css']);
    gulp.watch('src/' + env.p + '/fonts/**/*.scss', ['assets']);
});

gulp.task('default', function () {
    gulp.run('build');
});