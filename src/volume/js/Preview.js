import React from 'react';
import StateModel from './StateModel';

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        let containerClass = ['sign__preview', 'sign__preview--' + state.font];
        let textClass = ['sign__previewText', 'sign__previewText--char'];
        let frontHighlightStyle = {};
        let sideHighlightStyle = {};
        let backHighlightStyle = {};
        if (state.frontHighlight || state.diodes) {
            frontHighlightStyle.color = state.frontColor;
            frontHighlightStyle.textShadow = '1px 2px 2px ' + StateModel.Class.modifyColor(state.frontColor, 20) + ', 0 0 70px ' + state.frontColor;
        }
        if (state.sideHighlight) {
            sideHighlightStyle.color = state.sideColor;
            sideHighlightStyle.textShadow = '1px 2px 2px ' + StateModel.Class.modifyColor(state.sideColor, 20) + ', 7px 2px 10px ' + state.sideColor + ', 0 0 70px ' + StateModel.Class.modifyColor(state.sideColor, 20);
        }
        if (state.backHighlight) {
            backHighlightStyle.textShadow = '5px 0 15px #000000, 0 0 70px ' + state.frontColor;
        }
        return (
            <div className="sign__previewHolder" style={{ backgroundColor: state.backColor }}>
                <div className={ containerClass.join(' ') }>
                    <span className="sign__previewText" style={{ color: state.frontColor }}>{ state.text }</span>
                    { (state.diodes || state.frontHighlight) && <span className={ textClass.join(' ') } style={ frontHighlightStyle }>{ state.text }</span> }
                    { state.sideHighlight && <span className={ textClass.join(' ') } style={ sideHighlightStyle }>{ state.text }</span> }
                    { (new Array(15)).fill(0).map((i, index) => <span key={ index } className={ textClass.join(' ') } style={{ color: state.sideColor }}>{ state.text }</span> ) }
                    { state.backHighlight && <span className={ textClass.join(' ') } style={ backHighlightStyle }>{ state.text }</span> }
                    { state.sideHighlight && <span className={ textClass.join(' ') } style={ sideHighlightStyle }>{ state.text }</span> }
                </div>
            </div>
        );
    };

}