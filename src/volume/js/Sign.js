import React from 'react';
import Backbone from 'backbone';
import _ from 'underscore';
import Cookies from '../../lib/cookie';

import fonts from '../../data/fonts';
import State from './StateModel';
import { QueryModel } from '../../components/QueryModel';

import Preview from './Preview';
import Templates from './Templates';
import Options from './Options';


const templates = [
    {
        preview: '/static/sign-volume/img/1.png',
        description: (
            <div>
                <p>
                    Доступный  бюджетный вариант из "световых букв". Подходит для заведений, которые работают
                    в ночное время суток (магазины 24, кафе, аптеки)
                </p>
                <p>Лицевая сторона выполнена из молочного акрила и покрыта транслюцентной плёнкой ORACAL 8500.</p>
                <p>Толщина вывески – от 70 до 130 мм.</p>
                <p>Боковая и задняя стенки выполнены из пластика ПВХ. Подсветка выполнена светодиодными кластерами.</p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: true,
            sideColor: '#60aa45',
            sideHighlight: false,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/2.png',
        description: (
            <div>
                <p>Помимо наружных размещений, такие вывески часто располагаются внутри торговых центров.</p>
                <p>
                    Лицевая сторона выполнена из молочного акрила и покрыта транслюцентной плёнкой ORACAL 8500.
                    Толщина вывески – от 70 до 130 мм Боковая и задняя стенки выполнены из пластика ПВХ. Подсветка
                    выполнена светодиодными кластерами.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: true,
            sideColor: '#60aa45',
            sideHighlight: true,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/3.png',
        description: (
            <div>
                <p>
                    Вывеска смотрится заметнее, за счёт большого количества подсветки. Отличный выбор для
                    изготовления вывески в корпоративном стиле, в таком случае, контражур будет светиться
                    фирменным цветом вашей компании.
                </p>
                <p>
                    Лицевая сторона выполнена из молочного акрила и покрыта транслюцентной плёнкой ORACAL 8500.
                    Толщина вывески – от 70 до 130 мм Боковая и задняя стенки выполнены из пластика ПВХ. Подсветка
                    выполнена светодиодными кластерами.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: true,
            sideColor: '#60aa45',
            sideHighlight: false,
            backside: false,
            backColor: '#000000',
            backHighlight: true,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/4.png',
        description: (
            <div>
                <p>Отличный выбор для кафе, баров и ресторанов.</p>
                <p>Вывеска будет выделяться и днём и ночью.</p>
                <p>
                    Лицевая сторона выполнена из молочного акрила и покрыта транслюцентной плёнкой ORACAL 8500.
                    Толщина вывески – от 70 до 130 мм Боковая и задняя стенки выполнены из пластика ПВХ. Подсветка
                    выполнена светодиодными кластерами.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: true,
            sideColor: '#60aa45',
            sideHighlight: true,
            backside: false,
            backColor: '#000000',
            backHighlight: true,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/5.png',
        description: (
            <div>
                <p>
                    Отличный выбор для интерьерного решения, например, для размещения вывески внутри торгового
                    центра или для оформления витрин. Можно использовать в качестве декора
                </p>
                <p>
                    Лицевая сторона выполнена из пластика ПВХ и покрыта плёнкой ORACAL 641.
                    Толщина вывески – от 70 до 130 мм Боковая стенка выполнена из молочного акрила, задняя
                    стенка выполнена из пластика ПВХ. Подсветка выполнена светодиодными кластерами.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: false,
            sideColor: '#60aa45',
            sideHighlight: true,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/6.png',
        description: (
            <div>
                <p>
                    Самое яркое и заметное решение, благодаря открытым светодиодам ваша вывеска точно будет замечена.
                    Можно запрограммировать вывеску на интервальное мерцание.
                </p>
                <p>
                    Лицевая сторона выполнена из пластика ПВХ и покрыта плёнкой ORACAL 641.
                    Толщина вывески – от 50 до 150 мм Боковая и задняя стенки выполнены из пластика ПВХ.
                    Подсветка выполнена открытыми отдельными светодиодами повышенной яркости – SMD 3528
                    (любого цвета, на выбор).
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: true,
            sideColor: '#60aa45',
            sideHighlight: false,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: true
        }
    },
    {
        preview: '/static/sign-volume/img/7.png',
        description: (
            <div>
                <p>
                    Идеально подходит для компаний с дневным режимом работы. Доступный и заметный вариант.
                </p>
                <p>
                    Лицевая сторона выполнена из пластика ПВХ и покрыта плёнкой ORACAL 641.
                    Толщина вывески – от 50 до 150 мм. Боковая и задняя стенки выполнены из пластика ПВХ.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: false,
            sideColor: '#60aa45',
            sideHighlight: false,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: false
        }
    },
    {
        preview: '/static/sign-volume/img/8.png',
        description: (
            <div>
                <p>
                    Выбор премиум-класса. Глянцевый акрил с контражуром выгодно подчеркнет уровень вашего заведения.
                </p>
                <p>
                    Лицевая и боковая стороны выполнены из цветного глянцевого акрила.
                    Толщина вывески – от 50 до 150 мм. Задняя стенка выполнена из пластика ПВХ.
                    Подсветка выполнена светодиодными кластерами.
                </p>
            </div>
        ),
        options: {
            frontColor: '#fff',
            frontHighlight: false,
            sideColor: '#60aa45',
            sideHighlight: false,
            backside: false,
            backColor: '#000000',
            backHighlight: true,
            diodes: false
        }
    }
];

export default class extends React.Component {

    static defaultProps = {
        query: new QueryModel()
    };

    constructor(props) {
        super(props);

        State.model.set(this.props.query.attributes);

        this.state = {
            model: State.model
        };

        this.vent = _.extend({}, Backbone.Events);

        this.vent.listenTo(this.state.model, 'change', state => this.setState({ model: state }));
    };

    componentDidMount() {
        this.state.model.fetchPrice();
    };

    componentWillUnmount() {
        this.vent.off();
    };

    onChange(type, ev) {
        switch(type) {
            case 'frontColor':
            case 'sideColor':
            case 'backColor':
                State.model.set({ [type]: ev.hex });
                break;
            case 'checkbox':
                State.model.set({ [ev.target.name]: ev.target.checked });
                break;
            case 'selectFont':
                let font = fonts.filter(font => font.tag == ev.target.value)[0];
                State.model.set({
                    font: ev.target.value,
                    fontType: font.type
                });
                break;
            default:
                State.model.set({ [ev.target.name]: ev.target.value });
                break;
        }
    };

    onChangeTemplate(template) {
        this.state.model.set(template);
    };

    onSend(e) {
        e.preventDefault();
        Cookies.set('klukva-volume', JSON.stringify(this.state.model.toJSON()));
    };

    render() {
        let state = this.state.model.toJSON();
        return (
            <div>
                <div className="sign__calc">
                    <h2>Онлайн конструктор объемных букв</h2>

                    <div className="sign__container">
                        <input
                            type="text"
                            name="text"
                            className="sign__input sign__text"
                            placeholder="Введите вашу надпись"
                            value={ state.text == 'КЛЮКВА' ? '' : state.text }
                            onChange={ this.onChange.bind(this, 'input') } />
                        <Preview state={ state } />
                        <Templates templates={ templates } onChange={ this.onChangeTemplate.bind(this) } />
                    </div>

                    <Options state={ state } fonts={ fonts } onChange={ this.onChange.bind(this) } onSend={this.onSend.bind(this)} />
                </div>
            </div>
        );
    };

}