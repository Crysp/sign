import Backbone from 'backbone';

class StateModel extends Backbone.Model {

    constructor(attributes) {
        super(attributes);

        this.on('change:fontType change:frontHighlight change:sideHighlight change:backside change:backHighlight change:diodes change:fontSize', () => {
            if (this.changed.hasOwnProperty('diodes')) {
                this.set({ frontHighlight: false, sideHighlight: false });
            }
            this.fetchPrice()
        });
    };

    defaults() {
        return {
            text: 'КЛЮКВА',
            font: 'agencyb',
            fontType: 1,
            fontSize: 20,
            frontColor: '#ffffff',
            frontHighlight: false,
            sideColor: '#1165c3',
            sideHighlight: true,
            backside: false,
            backColor: '#000000',
            backHighlight: false,
            diodes: false,
            charPrice: 0,
            price: 0
        };
    };

    fetchPrice() {
        Backbone.ajax({
            url: 'http://klukva.red/api/getletterprice',
            method: 'get',
            data: {
                letter_count: this.get('text').replace(/\s+/g, '').length,
                letter_height: this.get('fontSize'),
                shrift: this.get('fontType'),
                lic: Number(this.get('frontHighlight')),
                bok: Number(this.get('sideHighlight')),
                podlojka: Number(this.get('backside')),
                kontrajur: Number(this.get('backHighlight')),
                svet: Number(this.get('diodes'))
            },
            success: (res) => {
                // let chars = this.get('text').replace(/\s+/g, '').length;
                this.set({
                    // charPrice: res.price,
                    price: res.price || 0
                })
            }
        });
    };

    static modifyColor(col, amt) {

        var usePound = false;

        if ((col == '#fff' || col == '#ffffff') && amt > 0) return col;

        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }

        var num = parseInt(col,16);

        var r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if  (r < 0) r = 0;

        var b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if  (b < 0) b = 0;

        var g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);

    };

}

let model = new StateModel();

export default {
    model,
    Class: StateModel
};