import React from 'react';
import { ChromePicker } from 'react-color';

class ColorPicker extends React.Component {

    state = {
        displayColorPicker: false
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '2'
        };
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        };
        let color = this.props.color;
        return (
            <div className="sign__colorWrapper">
                <button className="sign__colorButton" style={{ backgroundColor: color }} onClick={ this.handleClick }>Pick Color</button>
                { this.state.displayColorPicker ? <div style={ popover }>
                    <div style={ cover } onClick={ this.handleClose } />
                    <ChromePicker color={ color } onChangeComplete={ color => this.props.onChange(this.props.name, color) } />
                </div> : null }
            </div>
        )
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        let fonts = this.props.fonts;

        return (
            <div className="sign__sidebar">
                <div className="sign__optionTitle">Шрифт</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="font" className="sign__label">Тип шрифта</label>
                        <select
                            name="font"
                            id="font"
                            className="sign__select"
                            value={ state.font }
                            onChange={ ev => this.props.onChange('selectFont', ev) }>
                            { fonts.map(({ tag, title }) => <option key={ tag } value={ tag }>{ title }</option>) }
                        </select>
                    </div>
                    <div className="sign__row">
                        <label htmlFor="size" className="sign__label">Высота букв, см</label>
                        <input
                            type="text"
                            id="size"
                            name="fontSize"
                            className="sign__input"
                            value={ state.fontSize } onChange={ ev => this.props.onChange('input', ev) } />
                    </div>
                </div>
                <div className="sign__optionTitle">Лицевая сторона</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="front-color" className="sign__label">Цвет</label>
                        <ColorPicker name="frontColor" color={ state.frontColor } onChange={ this.props.onChange } />
                    </div>
                    <div className="sign__row">
                        <input
                            type="checkbox"
                            name="frontHighlight"
                            id="front-highlight"
                            className="sign__checkbox"
                            checked={ state.frontHighlight }
                            disabled={ state.diodes }
                            onChange={ ev => this.props.onChange('checkbox', ev) } />
                        <label htmlFor="front-highlight" className="sign__checkboxLabel">Подсветка</label>
                    </div>
                </div>
                <div className="sign__optionTitle">Боковая сторона</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="side-color" className="sign__label">Цвет</label>
                        <ColorPicker name="sideColor" color={ state.sideColor } onChange={ this.props.onChange } />
                    </div>
                    <div className="sign__row">
                        <input
                            type="checkbox"
                            name="sideHighlight"
                            id="side-highlight"
                            className="sign__checkbox"
                            checked={ state.sideHighlight }
                            disabled={ state.diodes }
                            onChange={ ev => this.props.onChange('checkbox', ev) } />
                        <label htmlFor="side-highlight" className="sign__checkboxLabel">Подсветка</label>
                    </div>
                </div>
                <div className="sign__optionTitle">Дополнительно</div>
                <div className="sign__optionContent sign__optionContent--last">
                    <div className="sign__row">
                        <label htmlFor="back-color" className="sign__label">Фон</label>
                        <ColorPicker name="backColor" color={ state.backColor } onChange={ this.props.onChange } />
                    </div>
                    <div className="sign__row">
                        <div className="sign__tooltip sign__tooltip--inOption sign__tooltip--small">
                            <div className="sign__tooltip__icon">?</div>
                            <div className="sign__tooltip__text">подсвечивается лицевая часть буквы отдельными светодиодами</div>
                        </div>
                        <input
                            type="checkbox"
                            name="diodes"
                            id="diodes"
                            className="sign__checkbox"
                            checked={ state.diodes }
                            onChange={ ev => this.props.onChange('checkbox', ev) } />
                        <label htmlFor="diodes" className="sign__checkboxLabel sign__checkboxLabel--withTooltip">
                            Открытые светодиоды
                        </label>
                    </div>
                    <div className="sign__row">
                        <div className="sign__tooltip sign__tooltip--inOption sign__tooltip--small">
                            <div className="sign__tooltip__icon">?</div>
                            <div className="sign__tooltip__text">жёсткая основа для крепления букв</div>
                        </div>
                        <input
                            type="checkbox"
                            name="backside"
                            id="backside"
                            className="sign__checkbox"
                            checked={ state.backside }
                            onChange={ ev => this.props.onChange('checkbox', ev) } />
                        <label htmlFor="backside" className="sign__checkboxLabel sign__checkboxLabel--withTooltip">
                            Подложка
                        </label>
                    </div>
                    <div className="sign__row">
                        <div className="sign__tooltip sign__tooltip--inOption sign__tooltip--small">
                            <div className="sign__tooltip__icon">?</div>
                            <div className="sign__tooltip__text">источник света располагается с обратной стороны буквы</div>
                        </div>
                        <input
                            type="checkbox"
                            name="backHighlight"
                            id="back-highlight"
                            className="sign__checkbox"
                            checked={ state.backHighlight }
                            onChange={ ev => this.props.onChange('checkbox', ev) } />
                        <label htmlFor="back-highlight" className="sign__checkboxLabel sign__checkboxLabel--withTooltip">Контражур</label>
                    </div>
                </div>
                <div style={{ marginTop: 20 }}>
                    <div className="sign__order">
                        <div className="sign__orderValue">Итого: <b>{ state.price } р.</b></div>
                        <div className="sign__orderTime">Сроки изготовления: 5 &mdash; 7 дней</div>
                        <button className="sign__button sign__button--large" onClick={this.props.onSend}>Оформить заказ</button>
                    </div>
                </div>
            </div>
        );
    };

}