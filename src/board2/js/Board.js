import _ from 'underscore';
import Backbone from 'backbone';
import React, { Component } from 'react';

import State from './StateModel';
import fonts from '../../data/fonts';
import materials from '../../data/materials2';
import { QueryModel } from '../../components/QueryModel';

import Options from './Options';
import BoxPreview from '../../components/BoxPreview';

const boxWidth = 490;
const boxHeight = 300;
const fontHeightPx = 24;

export default class extends React.Component {
    static defaultProps = {
        query: new QueryModel()
    };
    constructor(props) {
        super(props);
        State.model.set(this.props.query.attributes);
        this.state = { model: State.model, defaultFontHeight: Number(State.model.get('fontSize')) };
        this.vent = _.extend({}, Backbone.Events);
        this.vent.listenTo(this.state.model, 'change', model => this.setState({ model }));
        this.state.model.lines.map(line => this.vent.listenTo(line, 'change', () => this.forceUpdate()));
    };
    componentWillUnmount() {
        this.vent.off();
    };
    pxToCm(px) {
        return (this.state.defaultFontHeight / fontHeightPx) * px;
    };
    onChangeLine(index, value) {
        this.state.model.lines[index].set({ text: value });
    };
    onChange(name, value) {
        if (name === 'picture') {
            let image = new Image();
            image.src = value;
            image.onload = () => {
                this.state.model.set({
                    width: this.pxToCm(image.width),
                    height: this.pxToCm(image.height),
                    picture: value
                });
            }
        } else {
            this.state.model.set({ [name]: value });
        }
    };
    onMoveSign(index, x, y) {
        this.state.model.lines[index].set({ x, y });
    };
    render() {
        let state = this.state.model.toJSON();
        return (
            <div>
                <div className="sign__calc">
                    <h2>Онлайн конструктор табличек</h2>
                    <BoardPreview
                        state={ state }
                        defaultFontHeight={ this.state.defaultFontHeight }
                        onChange={ this.onChange.bind(this) }
                        onChangeLine={ this.onChangeLine.bind(this) }
                        onMoveSign={ this.onMoveSign.bind(this) } />
                    <BoardOptions state={ state } onChange={ this.onChange.bind(this) } />
                </div>
                <Order price={ state.price } />
            </div>
        );
    };
}

class BoardPreview extends Component {
    render() {
        const { width, height, fontSize, backColor, picture, lines } = this.props.state;
        return (
            <div className="sign__container">
                <BoxPreview
                    width={ boxWidth }
                    height={ boxHeight }
                    cmWidth={ width }
                    cmHeight={ height }
                    cmFontHeight={ fontSize }
                    defaultFontHeight={ this.props.defaultFontHeight }
                    background={ backColor }
                    picture={ picture }
                    lines={ lines }
                    state={ this.props.state /* ??? */ }
                    onChange={ this.props.onChange }
                    onChangeLine={ this.props.onChangeLine }
                    onMoveSign={ this.props.onMoveSign } />
            </div>
        );
    };
}

class BoardOptions extends Component {
    render() {
        return (
            <div className="sign__sidebar">
                <Options
                    state={ this.props.state }
                    fonts={ fonts }
                    materials={ materials }
                    onChange={ this.props.onChange } />
            </div>
        );
    };
}

class Order extends Component {
    render() {
        return (
            <div style={{ marginTop: 20 }}>
                <div className="sign__order">
                    <div className="sign__orderValue">Итого: <b><span>{ this.props.price }</span> р.</b></div>
                    <div className="sign__orderTime">Сроки изготовления: 5 &mdash; 7 дней</div>
                    <button className="sign__button sign__button--large">Оформить заказ</button>
                </div>
            </div>
        );
    }
}