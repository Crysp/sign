import Backbone from 'backbone';

class Line extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);
    };

    defaults() {
        return {
            text: '',
            x: 0,
            y: 0
        };
    };

}

class StateModel extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);

        this.lines = [
            new Line({
                text: 'Ваш',
                x: 165,
                y: 20
            }),
            new Line({
                text: 'текст',
                x: 165,
                y: 100
            }),
            new Line({
                text: 'на табличке',
                x: 165,
                y: 180
            })
        ];
    };

    defaults() {
        return {
            material: 'pvh',
            type: 'fullColored',
            font: 'agencyb',
            fontSize: 33, // cm
            color: '#ffffff',
            backColor: '#222',
            picture: null,
            width: 600, // cm
            height: 350, // cm
            price: 0
        };
    };

    toJSON() {
        let json = Backbone.Model.prototype.toJSON.call(this);
        json.lines = this.lines.map(line => line.toJSON());
        return json;
    };

}

let model = new StateModel();

export default {
    model
}