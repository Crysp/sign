export default [
    { uid: 'pvh', title: 'ПВХ', types: [
        { uid: 'fullColored', title: 'Полноцветная печать', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' },
        { uid: 'uf', title: 'УФ-печать', description: 'Описание' },
    ], depths: [3, 5] },
    { uid: 'acryl', title: 'Акрил', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' },
        { uid: 'uf', title: 'УФ-печать', description: 'Описание' },
    ], depths: [3, 5, 6, 8, 10] },
    { uid: 'wood', title: 'Дерево', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' },
        { uid: 'art', title: 'Художественная роспись', description: 'Описание' }
    ], depths: [3] }
];