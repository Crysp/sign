export default [
    { uid: 'pvh', title: 'ПВХ', types: [
        { uid: 'fullColored', title: 'Полноцветная печать', description: 'изображение печатается на самоклеящейся пленке' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'аппликация из цветной самоклеящейся пленки' },
        { uid: 'uf', title: 'УФ-печать', description: 'прямая печать на материале' },
    ], depths: [3, 5] },
    { uid: 'acryl', title: 'Акрил', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' },
        { uid: 'uf', title: 'УФ-печать', description: 'Описание' },
    ], depths: [3, 5, 6, 8, 10] },
    { uid: 'plastic', title: 'Двухслойный пластик', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' }
    ], depths: [1.5, 2.5, 3] },
    { uid: 'panel', title: 'Алюминиевые композитные панели', types: [
        { uid: 'fullColored', title: 'Полноцветная печать', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' }
    ], depths: [3] },
    { uid: 'plywood', title: 'Фанера', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' }
    ], depths: [4, 6] }
];