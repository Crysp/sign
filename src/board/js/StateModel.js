import Backbone from 'backbone';
import materials from '../../data/materials';

class Line extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);
    };

    defaults() {
        return {
            text: '',
            x: 0,
            y: 0
        };
    };

}

class StateModel extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);

        this.lines = [
            new Line({
                text: 'Ваш',
                x: 165,
                y: 10
            }),
            new Line({
                text: 'текст',
                x: 165,
                y: 60
            }),
            new Line({
                text: 'на табличке',
                x: 165,
                y: 110
            })
        ];

        this.on('change:width change:height change:type change:material change:depth', () => this.fetchPrice());
    };

    defaults() {
        return {
            material: 'pvh',
            type: 'fullColored',
            font: 'agencyb',
            fontSize: 20, // cm
            color: '#000',
            backColor: '#e8e8e8',
            picture: null,
            width: 50, // сm
            height: 20, // сm
            depth: 0,
            price: 0
        };
    };

    fetchPrice() {
        Backbone.ajax({
            url: 'http://klukva.red/api/gettable',
            method: 'get',
            data: {
                width: parseFloat(this.get('width')),
                height: parseFloat(this.get('height')),
                type: this.get('type'),
                material: this.get('material'),
                thickness: Number(this.get('depth'))
            },
            success: (res) => {
                this.set({
                    price: res.price || 0
                })
            }
        });
    };

    toJSON() {
        let json = Backbone.Model.prototype.toJSON.call(this);
        json.lines = this.lines.map(line => line.toJSON());
        return json;
    };

}

let model = new StateModel();

export default {
    model
}