import React, { Component } from 'react';
import OptionsGroup, { Header, Options, Field } from '../../components/Options';

const normalizeFonts = (fonts) => { return fonts.map(font => ({ ...font, value: font.tag })) };
const normalizeMaterials = (materials) => { return materials.map(material => ({ ...material, value: material.uid })) };
const normalizeTypes = (types) => { return types.map(type => ({ ...type, value: type.uid })) };
const normalizeDepths = (depths) => { return depths.map(depth => ({ value: depth, title: depth })) };

class FontOptions extends Component {
    render() {
        return (
            <Options>
                <Field
                    label="Тип шрифта"
                    type="select"
                    items={ this.props.fonts }
                    defaultValue={ this.props.selectedFont }
                    onChange={ value => this.props.onChange('font', value) } />
                <Field
                    label="Цвет"
                    type="color"
                    defaultValue={ this.props.selectedColor }
                    onChange={ value => this.props.onChange('color', value) } />
            </Options>
        );
    }
}

class BoardOptions extends Component {
    render() {
        return (
            <Options>
                <Field
                    label="Материал"
                    type="select"
                    items={ this.props.materials }
                    defaultValue={ this.props.selectedMaterial }
                    onChange={ value => this.props.onChange('material', value) } />
                <Field
                    label="Толщина, мм"
                    type="select"
                    items={ this.props.depths }
                    defaultValue={ this.props.selectedDepth }
                    onChange={ value => this.props.onChange('depth', value) } />
                <Field
                    label="Цвет"
                    type="color"
                    defaultValue={ this.props.selectedBackColor }
                    onChange={ value => this.props.onChange('backColor', value) } />
                <p style={{ textAlign: 'center', margin: '10px 0 10px 0' }}>или</p>
                <Field
                    label="Картинка"
                    type="file"
                    defaultValue={ this.props.selectedPicture }
                    onChange={ value => this.props.onChange('picture', value) } />
            </Options>
        );
    }
}

export default class extends Component {
    constructor(props) {
        super(props);
    };
    onChangeOption(name, value) {
        this.props.onChange(name, value);
    };
    getTypes() {
        const selectedMaterial = this.props.materials.filter(material => material.uid === this.props.state.material);
        return selectedMaterial && selectedMaterial.length ? selectedMaterial[0].types : null;
    };
    getDepths() {
        const selectedMaterial = this.props.materials.filter(material => material.uid === this.props.state.material);
        return selectedMaterial && selectedMaterial.length ? selectedMaterial[0].depths : null;
    };
    render() {
        const state = this.props.state;
        const fonts = normalizeFonts(this.props.fonts);
        const materials = normalizeMaterials(this.props.materials);
        const types = normalizeTypes(this.getTypes());
        const depths = normalizeDepths(this.getDepths());
        return (
            <OptionsGroup>
                <Header>Параметры таблички</Header>
                <BoardOptions
                    materials={ materials }
                    depths={ depths }
                    selectedMaterial={ state.material }
                    selectedDepth={ state.depth }
                    selectedBackColor={ state.backColor }
                    selectedPicture={ state.picture }
                    onChange={ this.onChangeOption.bind(this) } />
                <Header>Способ нанесения изображения</Header>
                <Options>
                    <Field
                        type="buttons"
                        items={ types }
                        defaultValue={ state.type }
                        onChange={ this.onChangeOption.bind(this, 'type') } />
                </Options>
                <Header>Шрифт</Header>
                <FontOptions
                    fonts={ fonts }
                    selectedFont={ state.font }
                    selectedSize={ state.fontSize }
                    selectedColor={ state.color }
                    onChange={ this.onChangeOption.bind(this) }/>
            </OptionsGroup>
        );
    };
}