import React from 'react';

class Template extends React.Component {

    render() {
        let template = this.props.template;
        return (
            <div className="sign__templateWrapper">
                <button
                    className="sign__template"
                    onClick={ () => this.props.onClick(template.options) }>
                    <img src={ template.preview } />
                </button>
            </div>
        );
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTpl: 0
        };
    };

    render() {
        let templates = this.props.templates;
        let selected = templates.filter((template, index) => index === this.state.selectedTpl)[0];
        return (
            <div className="sign__templateListWrapper">
                <div className="sign__templateHeader">Готовые шаблоны</div>
                <div className="sign__templatePreview">
                    <img src={ selected.preview } className="sign__templatePreviewImage" />
                    { selected.description }
                </div>
                <div className="sign__templateList">
                    { templates.map((template, index) => <Template key={ index } template={ template } onClick={ () => {
                        this.setState({ selectedTpl: index });
                        this.props.onChange(template.options)
                    } } />) }
                </div>
            </div>
        );
    };

}