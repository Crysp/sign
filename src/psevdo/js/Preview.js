import React from 'react';
import StateModel from './StateModel';

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        let containerClass = ['sign__preview', 'sign__preview--' + state.font];
        let textClass = ['sign__previewText', 'sign__previewText--char'];
        return (
            <div className="sign__previewHolder" style={{ backgroundColor: state.backColor }}>
                <div className={ containerClass.join(' ') }>
                    <span className="sign__previewText" style={{ color: state.color }}>{ state.text }</span>
                    { (new Array(15)).fill(0).map((i, index) => <span key={ index } className={ textClass.join(' ') } style={{ color: state.sideColor }}>{ state.text }</span> ) }
                </div>
            </div>
        );
    };

}