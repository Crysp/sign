import React from 'react';
import { ChromePicker } from 'react-color';

class ColorPicker extends React.Component {

    state = {
        displayColorPicker: false
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '2'
        };
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        };
        let color = this.props.color;
        return (
            <div className="sign__colorWrapper">
                <button className="sign__colorButton" style={{ backgroundColor: color }} onClick={ this.handleClick }>Pick Color</button>
                { this.state.displayColorPicker ? <div style={ popover }>
                    <div style={ cover } onClick={ this.handleClose } />
                    <ChromePicker color={ color } onChangeComplete={ color => this.props.onChange(this.props.name, color) } />
                </div> : null }
            </div>
        )
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        let fonts = this.props.fonts;
        let materials = this.props.materials;
        let material = materials.filter(material => material.tag === state.material)[0];
        return (
            <div className="sign__sidebar">
                <div className="sign__optionTitle">Шрифт</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="font" className="sign__label">Тип шрифта</label>
                        <select
                            name="font"
                            id="font"
                            className="sign__select"
                            value={ state.font }
                            onChange={ ev => this.props.onChange('selectFont', ev) }>
                            { fonts.map(({ tag, title }) => <option key={ tag } value={ tag }>{ title }</option>) }
                        </select>
                    </div>
                    <div className="sign__row">
                        <label htmlFor="size" className="sign__label">Высота букв, см</label>
                        <input
                            type="text"
                            id="size"
                            name="fontSize"
                            className="sign__input"
                            value={ state.fontSize } onChange={ ev => this.props.onChange('input', ev) } />
                    </div>
                </div>
                <div className="sign__optionTitle">Цвет и материал</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="material" className="sign__label">Материал</label>
                        <select
                            name="material"
                            id="material"
                            className="sign__select"
                            value={ state.material }
                            onChange={ ev => this.props.onChange('select', ev) }>
                            { materials.map(({ tag, title }) => <option key={ tag } value={ tag }>{ title }</option>) }
                        </select>
                    </div>
                    { state.material === 'acryl' && (
                        <div className="sign__row">
                            <input
                                type="checkbox"
                                name="transparent"
                                id="transparent"
                                className="sign__checkbox"
                                checked={ state.transparent }
                                onChange={ ev => this.props.onChange('checkbox', ev) } />
                            <label htmlFor="transparent" className="sign__checkboxLabel">Прозрачный</label>
                        </div>
                    ) }
                    <div className="sign__row">
                        <label htmlFor="material" className="sign__label">Толщина</label>
                        <select
                            name="depth"
                            id="depth"
                            className="sign__select"
                            value={ state.depth }
                            onChange={ ev => this.props.onChange('select', ev) }>
                            { material.depth.map(value => <option key={ value } value={ value }>{ value }</option>) }
                        </select>
                    </div>
                    { (state.material === 'acryl' || state.material === 'wood' || state.material === 'pvh') && (
                        <div className="sign__row">
                            <input
                                type="checkbox"
                                name="colored"
                                id="colored"
                                className="sign__checkbox"
                                checked={ state.colored }
                                onChange={ ev => this.props.onChange('checkbox', ev) } />
                            <label htmlFor="colored" className="sign__checkboxLabel">Цветной</label>
                        </div>
                    ) }
                    { ((state.material !== 'wood' && state.material !== 'pvh') || state.colored) && (
                        <div className="sign__row">
                            <label htmlFor="color" className="sign__label">Цвет</label>
                            <ColorPicker name="color" color={ state.color } onChange={ this.props.onChange } />
                        </div>
                    ) }
                </div>
                <div className="sign__optionTitle">Фон</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="back-color" className="sign__label">Цвет</label>
                        <ColorPicker name="backColor" color={ state.backColor } onChange={ this.props.onChange } />
                    </div>
                </div>
            </div>
        );
    };

}