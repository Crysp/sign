import React from 'react';
import Backbone from 'backbone';
import _ from 'underscore';
import Cookies from '../../lib/cookie';

import fonts from '../../data/fonts';
import State from './StateModel';
import { QueryModel } from '../../components/QueryModel';

import Preview from './Preview';
import Templates from './Templates';
import Options from './Options';

const materials = [
    { tag: 'acryl_transparent', title: 'Акрил (прозрачный)', depth: [3, 5, 8, 10], color: '#CFF1F9' },
    { tag: 'acryl_colored', title: 'Акрил (цветной)', depth: [3, 5, 8], color: '#60aa45' },
    { tag: 'pvh', title: 'ПВХ', depth: [3, 5, 8, 10], color: '#ffffff' },
    { tag: 'wood', title: 'Дерево', depth: [3, 4, 6], color: '#c1ab80' }
];

const templates = [ ///static/sign-calc/img/ /sign/build/img/
    {
        preview: '/static/sign-psevdo/img/1.png',
        description: 'Прозрачные буквы чаще всего заказывают в качестве заготовок для дальнейшего использования. Метод обработки - лазерная резка.',
        options: {
            material: 'acryl_transparent',
            colored: false,
            color: '#CFF1F9',
            sideColor: State.Class.modifyColor('#CFF1F9', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/2.png',
        description: 'Лицевая поверхность буквы покрыта пленкой ORACAL 641 или транслюцентной пленкой ORACAL 8500). Метод обработки – лазерная  резка. Буквы идеально подходят в качестве интерьерных вывесок',
        options: {
            material: 'acryl_transparent',
            colored: true,
            color: '#60aa45',
            sideColor: State.Class.modifyColor('#CFF1F9', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/3.png',
        description: 'Буквы выполнены из цветного акрилового стекла',
        options: {
            material: 'acryl_colored',
            colored: true,
            color: '#60aa45',
            sideColor: State.Class.modifyColor('#60aa45', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/4.png',
        description: 'Лёгкие и простые в использование буквы из белого пластика',
        options: {
            material: 'pvh',
            colored: false,
            color: '#ffffff',
            sideColor: State.Class.modifyColor('#ffffff', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/5.png',
        description: 'Лёгкие буквы из белого ПВХ, покрытые цветной плёнкой ORACAL 641',
        options: {
            material: 'pvh',
            colored: true,
            color: '#60aa45',
            sideColor: State.Class.modifyColor('#ffffff', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/6.png',
        description: 'Частый атрибут праздников и фотосессий. Метод обработки – лазерная резка.',
        options: {
            material: 'wood',
            colored: false,
            color: '#c1ab80',
            sideColor: State.Class.modifyColor('#c1ab80', -30)
        }
    },
    {
        preview: '/static/sign-psevdo/img/7.png',
        description: 'Деревянные буквы, покрытые матовой акриловой краской.',
        options: {
            material: 'wood',
            colored: true,
            color: '#60aa45',
            sideColor: State.Class.modifyColor('#60aa45', -30)
        }
    }
];

export default class extends React.Component {

    static defaultProps = {
        query: new QueryModel()
    };

    constructor(props) {
        super(props);

        State.model.set(this.props.query.attributes);

        this.state = {
            model: State.model
        };

        this.vent = _.extend({}, Backbone.Events);

        this.vent.listenTo(State.model, 'change', state => this.setState({ model: state }));
    };

    componentDidMount() {
        this.state.model.fetchPrice();
    };

    componentWillUnmount() {
        this.vent.off();
    };

    onChange(type, ev) {
        switch(type) {
            case 'color':
            case 'backColor':
                State.model.set({ [type]: ev.hex });
                break;
            case 'checkbox':
                State.model.set({ [ev.target.name]: ev.target.checked });
                break;
            case 'selectFont':
                let font = fonts.filter(font => font.tag == ev.target.value)[0];
                State.model.set({
                    font: ev.target.value,
                    fontType: font.type
                });
                break;
            default:
                if (ev.target.name == 'material') {
                    let m = materials.filter(material => material.tag === ev.target.value)[0];
                    if (m.color) State.model.set({ color: m.color, sideColor: State.Class.modifyColor(m.color, -30) }, { silent: true });
                    State.model.set({ colored: false }, { silent: true });
                }
                State.model.set({ [ev.target.name]: ev.target.value });
                break;
        }
    };

    onChangeTemplate(template) {
        this.state.model.set(template);
    };

    onSend(e) {
        e.preventDefault();
        Cookies.set('klukva-psevdo', JSON.stringify(this.state.model.toJSON()));
    };

    render() {
        let state = this.state.model.toJSON();
        return (
            <div>
                <h2>Онлайн конструктор псевдообъемных букв</h2>

                <div className="sign__container">
                    <input
                        type="text"
                        name="text"
                        className="sign__input sign__text"
                        placeholder="Введите вашу надпись"
                        value={ state.text != 'КЛЮКВА' ? state.text : '' }
                        onChange={ this.onChange.bind(this, 'input') } />
                    <Preview state={ state } />
                </div>

                <Options state={ state } fonts={ fonts } materials={ materials } onChange={ this.onChange.bind(this) } />

                <div className="sign__additional">
                    <Templates templates={ templates } onChange={ this.onChangeTemplate.bind(this) } />
                    <div className="sign__order">
                        <div className="sign__orderValue">Итого: <b>{ state.price } р.</b></div>
                        <div className="sign__orderTime">Сроки изготовления: 5 &mdash; 7 дней</div>
                        <button className="sign__button sign__button--large" onClick={this.onSend.bind(this)}>Оформить заказ</button>
                    </div>
                </div>

            </div>
        );
    };

}