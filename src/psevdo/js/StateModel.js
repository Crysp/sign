import Backbone from 'backbone';

class StateModel extends Backbone.Model {

    constructor(attributes) {
        super(attributes);
        this.on('change:color', () => {
            if (['acryl_transparent', 'pvh'].indexOf(this.get('material')) === -1) {
                this.set({ sideColor: StateModel.modifyColor(this.get('color'), -30) }, { silent: true });
            }
        });
        this.on('change:font change:fontSize change:material change:depth change:colored', () => {
            if (this.changed.hasOwnProperty('diodes')) {
                this.set({ frontHighlight: false, sideHighlight: false });
            }
            this.fetchPrice()
        });
    };

    defaults() {
        return {
            text: 'КЛЮКВА',
            font: 'agencyb',
            fontType: 1,
            fontSize: 2,
            material: 'acryl_transparent',
            transparent: false,
            colored: false,
            depth: 3,
            color: '#CFF1F9',
            sideColor: StateModel.modifyColor('#CFF1F9', -30),
            backColor: '#000000',
            charPrice: 0,
            price: 0
        };
    };

    fetchPrice() {
        Backbone.ajax({
            url: 'http://klukva.red/api/psevdo',
            method: 'get',
            data: {
                letter_count: this.get('text').replace(/\s+/g, '').length,
                letter_height: this.get('fontSize'),
                material: this.get('material'),
                thickness: this.get('depth'),
                colored: this.get('colored') ? 1 : 0
            },
            success: (res) => {
                this.set({ price: res.price || 0 });
            }
        });
    };

    static modifyColor(color, percent) {
        var R = parseInt(color.substring(1,3),16);
        var G = parseInt(color.substring(3,5),16);
        var B = parseInt(color.substring(5,7),16);

        R = parseInt(R * (100 + percent) / 100);
        G = parseInt(G * (100 + percent) / 100);
        B = parseInt(B * (100 + percent) / 100);

        R = (R<255)?R:255;
        G = (G<255)?G:255;
        B = (B<255)?B:255;

        var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
        var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
        var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

        return "#"+RR+GG+BB;
    };

    static getBounds() {
        return {

        };
    };

}

let model = new StateModel();

export default {
    model,
    Class: StateModel
};