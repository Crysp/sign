import React, { Component } from 'react';
import css from './staticPreview.scss';


const leftOffset = 25;
const rightOffset = 25;
const bottomOffset = 25;
const topOffset = 15;
const fontSizePx = 24;

class Axis extends Component {
    render() {
        let classes = [css.axis];
        if (this.props.direction === 'y') classes.push(css.axisY);
        else classes.push(css.axisX);
        return (
            <div className={ classes.join(' ') }>
                <div className={ css.axisFieldWrapper }>
                    <input type="number" className={ css.axisField } value={ this.props.value } onChange={ ev => this.props.onChange(Number(ev.target.value)) }/>
                    <span className={ css.axisLabel }>{this.props.units}</span>
                </div>
            </div>
        );
    }
}

class Box extends Component {
    render() {
        const style = {
            width: `${this.props.width}px`,
            height: `${this.props.height}px`,
            backgroundImage: `url(${this.props.picture})`
        };
        return (
            <div className={ css.box } style={ style }></div>
        );
    };
}

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultFontHeight: props.defaultFontHeight
        };
    };
    onChangeWidth(width) {
        this.props.onChange('width', width);
    };
    onChangeHeight(height) {
        this.props.onChange('height', height);
    }
    getBounds() {
        return {
            boundWidth: Number(this.props.width) - leftOffset - rightOffset,
            boundHeight: Number(this.props.height) - bottomOffset - topOffset
        };
    };
    cmToPx(cm) {
        return (fontSizePx / this.state.defaultFontHeight) * cm;
    };
    render() {
        const { boundWidth, boundHeight } = this.getBounds();
        const style = {
            width: `${boundWidth}px`,
            height: `${boundHeight}px`,
            padding: `${topOffset}px ${rightOffset}px ${bottomOffset}px ${leftOffset}px`
        };
        return (
            <div className={ css.wrapper } style={ style }>
                <Axis direction="y" value={ this.props.cmHeight } units={this.props.units} onChange={ this.onChangeHeight.bind(this) } />
                <Axis direction="x" value={ this.props.cmWidth } units={this.props.units} onChange={ this.onChangeWidth.bind(this) } />
                <Box picture={ this.props.picture } />
            </div>
        );
    }
}