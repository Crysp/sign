import React, { Component } from 'react';
import { ChromePicker } from 'react-color';
import css from './options.scss';


export default class extends Component {
    render() {
        return (
            <div className={ css.wrapper }>
                { this.props.children }
            </div>
        );
    }
}

export class Header extends Component {
    render() {
        return (
            <div className={ css.header }>{ this.props.children }</div>
        );
    }
}

export class Options extends Component {
    render() {
        return (
            <div className={ css.content }>{ this.props.children }</div>
        );
    }
}

export class Field extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue
        };
    };
    getHTMLField() {
        switch (this.props.type) {
            case 'input':
                return <input type="text" id={ this.props.label } className={ css.input } value={ this.state.value } onChange={ this.onChange.bind(this) } />;
            case 'select':
                return (
                    <select
                        id={ this.props.label }
                        className={ css.select }
                        value={ this.state.value }
                        onChange={ this.onChange.bind(this) }>
                        { this.props.items.map(({ value, title }) => <option key={ value } value={ value }>{ title }</option>) }
                    </select>
                );
            case 'color':
                return <ColorPicker color={ this.props.defaultValue || this.state.value } onChange={ this.onChange.bind(this) } />;
            case 'file':
                return <FileButton onChange={ this.onChange.bind(this) } />;
            case 'buttons':
                return this.props.items.map(item => <TypeButton key={ item.value } item={ item } isActive={ this.state.value === item.value } onChange={ this.onChange.bind(this) } />);
        }
    };
    onChange(ev) {
        switch (this.props.type) {
            case 'file':
            case 'buttons':
            case 'color':
                this.setState({ value: ev });
                this.props.onChange(ev);
                break;
            default:
                this.setState({ value: ev.target.value });
                this.props.onChange(ev.target.value);
        }
    };
    render() {
        return (
            <div className={ css.row }>
                <label htmlFor={ this.props.label } className={ css.label }>{ this.props.label }</label>
                { this.getHTMLField() }
            </div>
        );
    }
}

class ColorPicker extends Component {

    state = {
        displayColorPicker: false
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '300',
            right: 18
        };
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        };
        let color = this.props.color;
        return (
            <div className="sign__colorWrapper">
                <button className="sign__colorButton" style={{ backgroundColor: color }} onClick={ this.handleClick }>Pick Color</button>
                { this.state.displayColorPicker ? <div style={ popover }>
                    <div style={ cover } onClick={ this.handleClose } />
                    <ChromePicker color={ color } onChangeComplete={ color => this.props.onChange(color) } />
                </div> : null }
            </div>
        )
    };

}

class FileButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null
        };
    };
    onOpenFileDialog(ev) {
        this.refs.nativeFile.click();
    };
    onSelectFile() {
        const reader = new FileReader();
        const file = this.refs.nativeFile.files[0];
        reader.addEventListener('load', () => this.props.onChange(reader.result), false);
        if (file) reader.readAsDataURL(file);
    };
    render() {
        return (
            <div className={ css.buttonWrapper }>
                <button className={ `${css.button} ${css.buttonHollow}` } onClick={ this.onOpenFileDialog.bind(this) }>Загрузить</button>
                <input ref="nativeFile" type="file" className={ css.hidden } onChange={ this.onSelectFile.bind(this) } />
            </div>
        );
    };

}

class TypeButton extends Component {
    render() {
        let item = this.props.item;
        let isActive = this.props.isActive;
        let itemClasses = ['sign__button sign__button--hollow sign__button--block sign__button--attached'];
        if (isActive) itemClasses.push('sign__button--active');
        return (
            <div className="sign__typeButtonWrapper">
                <button className={ itemClasses.join(' ') } onClick={ ev => this.props.onChange(item.value) }>{ item.title }</button>
                { isActive && item.description && <div className="sign__buttonDescription">{ item.description }</div> }
            </div>
        );
    };
}