import React, { Component } from 'react';
import EditableText from '../EditableText';
import Color from '../../lib/color';
import css from './boxPreview.scss';


const leftOffset = 25;
const rightOffset = 25;
const bottomOffset = 25;
const topOffset = 15;
const fontSizePx = 24;

class Axis extends Component {
    render() {
        const { units } = this.props;
        const classes = [css.axis];
        if (this.props.direction === 'y') classes.push(css.axisY);
        else classes.push(css.axisX);
        return (
            <div className={ classes.join(' ') }>
                <div className={ css.axisFieldWrapper }>
                    <input type="number" className={ css.axisField } value={ this.props.value } onChange={ ev => this.props.onChange(Number(ev.target.value)) }/>
                    <span className={ css.axisLabel }>{units ? units : 'см'}</span>
                </div>
            </div>
        );
    }
}

class Box extends Component {
    render() {
        const wrapperStyle = {
            width: `${this.props.width}px`,
            height: `${this.props.height}px`
        };
        const boxStyle = {
            backgroundColor: this.props.background,
            backgroundImage: this.props.picture && `url(${this.props.picture})`,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 50%'
        };
        const boxVolumeStyle = {
            borderBottomColor: Color.modifyColor(this.props.background, -10)
        };
        return (
            <div className={ css.boxWrapper } style={ wrapperStyle }>
                <div className={ css.box } style={ boxStyle }>
                    { this.props.children }
                </div>
                {this.props.volume ? <div className={ css.boxShadow }></div> : ''}
                {this.props.volume ? <div className={ css.boxVolume } style={ boxVolumeStyle }></div> : ''}
            </div>
        );
    };
}

export default class extends Component {
    static defaultProps = {
        volume: true
    };
    constructor(props) {
        super(props);
        this.state = {
            defaultFontHeight: props.defaultFontHeight
        };
    };
    onChangeWidth(width) {
        this.props.onChange('width', width);
    };
    onChangeHeight(height) {
        this.props.onChange('height', height);
    }
    getRatio() {
        const { units } = this.props;
        const _width = this.cmToPx(units ? Number(parseFloat(this.props.cmWidth) * 100) : Number(this.props.cmWidth));
        const _height = this.cmToPx(units ? Number(parseFloat(this.props.cmHeight) * 100) : Number(this.props.cmHeight));
        const targetWidth = this.props.width;
        const targetHeight = this.props.height;
        let pxFontHeight = this.cmToPx(this.props.cmFontHeight);
        let adjustedWidth = 0;
        let adjustedHeight = 0;
        let adjustedFont = 0;
        const boxAspectRatio = _width/_height;
        const targetAspectRatio = targetWidth/targetHeight;
        adjustedWidth = targetWidth;
        adjustedHeight = targetHeight;
        if (boxAspectRatio > targetAspectRatio) {
            adjustedHeight = targetWidth / boxAspectRatio;
        } else {
            adjustedWidth = targetHeight * boxAspectRatio;
        }
        adjustedFont = pxFontHeight * boxAspectRatio;
        return {
            width: adjustedWidth,
            height: adjustedHeight,
            fontSize: 24
        };
    };
    getBounds() {
        return {
            boundWidth: Number(this.props.width) - leftOffset - rightOffset + 40,
            boundHeight: Number(this.props.height) - bottomOffset - topOffset + 40
        };
    };
    cmToPx(cm) {
        return (fontSizePx / this.state.defaultFontHeight) * cm;
    };
    render() {
        const { units } = this.props;
        const { boundWidth, boundHeight } = this.getBounds();
        const style = {
            width: `${boundWidth}px`,
            height: `${boundHeight}px`,
            padding: `${topOffset}px ${rightOffset}px ${bottomOffset}px ${leftOffset}px`,
            marginTop: '60px'
        };
        const { width, height, fontSize } = this.getRatio();
        return (
            <div className={ css.wrapper } style={ style }>
                <Axis direction="y" value={ this.props.cmHeight } units={units} onChange={ this.onChangeHeight.bind(this) } />
                <Axis direction="x" value={ this.props.cmWidth } units={units} onChange={ this.onChangeWidth.bind(this) } />
                <Box width={ width } height={ height } volume={this.props.volume} background={ this.props.background } picture={ this.props.picture }>
                    { this.props.lines.map((line, i) => (
                        <EditableText
                            key={ i }
                            index={ i }
                            line={ line }
                            state={ this.props.state }
                            contextWidth={ width }
                            contextHeight={ height }
                            fontSize={ fontSize }
                            onChange={ this.props.onChangeLine }
                            onMoveSign={ this.props.onMoveSign } />
                    )) }
                </Box>
            </div>
        );
    }
}