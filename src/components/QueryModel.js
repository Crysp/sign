import Backbone, { Model } from 'backbone';


export class QueryModel extends Model {
    initialize() {
        const queries = window.location.search.slice(1).split('&');
        const queryMap = {};
        queries.forEach(query => {
            const keys = query.split('=');
            queryMap[keys[0]] = keys[1];
        });
        this.set(queryMap);
    };
}