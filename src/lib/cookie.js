export default class {

    static set(name, value, expireDays, path = '/') {
        let expire = new Date();
        expire.setTime(expire.getTime() + (expireDays * 24 * 60 * 60 * 1000));
        document.cookie = `${name}=${value}; path=${path}; expires=${expire.toUTCString()}`
    };

    static get(name) {
        let _name = `${name}=`;
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(_name) == 0) {
                return c.substring(_name.length, c.length);
            }
        }
        return '';
    };

    static remove(name) {
        document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
    }

}