import _ from 'underscore';
import Backbone from 'backbone';
import React from 'react';
import Cookies from '../../lib/cookie';

import State from './StateModel';
import fonts from '../../data/fonts';
import { QueryModel } from '../../components/QueryModel';

import Options from './Options';
import StaticPreview from '../../components/StaticPreview';

const boxWidth = 490;
const boxHeight = 300;

const types = [
    {
        index: 1,
        title: 'Премиум',
        description: 'Респектабельный короб премиум-класса. Представляет собой композитную панель с инкрустацией ' +
        'акрилом. Тонкий световой короб (от 50 мм), украшенный объёмными световыми буквами и инкрустацией акрилом. ' +
        'Может быть декорирован элементами из зеркального или цветного акрила.',
        image: '/static/sign-lightbox/img/1.png',
        sub: []
    },
    {
        index: 2,
        title: 'Фигурный',
        description: 'Короб повторяет форму названия вашей компании или логотипа. Лицевая часть  выполнена из ' +
        'молочного акрила, изображение наносится при помощи специальной плёнки либо с помощью полноцветной печати. ' +
        'Подсветка выполнена светодиодными кластерами с длительным и экономичным сроком эксплуатации.',
        image: '/static/sign-lightbox/img/2.png',
        sub: []
    },
    {
        index: 3,
        title: 'Стандарт',
        sub: [
            {
                index: 4,
                title: '1',
                description: 'Экономичный и практичный вариант. В основе - алюминиевая сварная конструкция. Лицевая ' +
                'панель выполнена из молочного акрила. Изображение наносится пленкой ORACAL 8100/8500 или ' +
                'полноцветной печатью. Боковая часть выполнена из алюминиевого профиля 130 мм толщиной. Подсветка ' +
                'выполнена светодиодными кластерами с длительным и экономичным сроком эксплуатации.',
                image: '/static/sign-lightbox/img/3.png',
            },
            {
                index: 5,
                title: '2',
                description: 'Экономичный и практичный вариант. В основе - алюминиевая сварная конструкция. Лицевая ' +
                'панель выполнена из молочного акрила. Изображение наносится пленкой ORACAL 8100/8500 или ' +
                'полноцветной печатью. Боковая часть выполнена из алюминиевого профиля 180 мм толщиной. Подсветка ' +
                'выполнена светодиодными кластерами с длительным и экономичным сроком эксплуатации.',
                image: '/static/sign-lightbox/img/4.png'
            },
            {
                index: 6,
                title: '3',
                description: 'Один из самых популярный коробов в наружной рекламе. Выполнен полностью из ПВХ. ' +
                'Если короб световой. лицевая частью выполняется из акрила. Такой тип короба можно изготовить ' +
                'крупногабаритным, в таком случае, прочность придаётся при помощи специальной сварной конструкции.',
                image: '/static/sign-lightbox/img/5.png'
            },
            {
                index: 7,
                title: '4',
                description: 'Самый яркий и востребованный на рынке тип короба. Основа выполнена из ПВХ, лицевая ' +
                'часть покрыта аппликацией из пленки, сверху на которую монтируются светодиоды с длительным сроком ' +
                'эксплуатации, подчёркивая название вашей компании. Вас точно заметят в любое время суток.',
                image: '/static/sign-lightbox/img/6.png'
            }
        ]
    },
    {
        index: 8,
        title: 'Эконом',
        description: 'Наиболее доступный вариант вывески эконом-класса. Основа короба выполнена из ПВХ, боковая ' +
        'часть выполнена из алюминиевого профиля 130 мм, на лицевой части располагается сотовый поликарбонат, на ' +
        'который наносится полноцветная печать или плёночная аппликация.',
        image: '/static/sign-lightbox/img/7.png',
        sub: []
    }
];

const getPicture = (typeIndex) => {
    let _type = null;
    types.forEach(type => {
        if (type.index == typeIndex) _type = type;
        else if (type.sub.length) {
            type.sub.forEach(subType => {
                if (subType.index == typeIndex) _type = subType;
            });
        }
    });
    return _type ? _type.image : null;
};

export default class extends React.Component {

    static defaultProps = {
        query: new QueryModel()
    };

    constructor(props) {
        super(props);

        State.model.set(this.props.query.attributes);

        this.state = { model: State.model };

        this.vent = _.extend({}, Backbone.Events);

        this.vent.listenTo(this.state.model, 'change', model => this.setState({ model }));
    };

    componentDidMount() {
        this.state.model.fetchPrice();
    };

    componentWillUnmount() {
        this.vent.off();
    };

    onChange(type, i, value) {
        if (typeof value == 'undefined') value = i;
        switch(type) {
            default:
                this.state.model.set({ [type]: value });
                break;
        }
    };

    onSend(e) {
        e.preventDefault();
        Cookies.set('klukva-lightbox', JSON.stringify(this.state.model.toJSON()));
    };

    render() {
        let state = this.state.model.toJSON();
        return (
            <div>
                <div className="sign__calc">
                    <h2>Онлайн конструктор светового короба</h2>

                    <div className="sign__container" style={{ marginTop: 20 }}>
                        <StaticPreview
                            width={ boxWidth }
                            height={ boxHeight }
                            cmWidth={ state.width }
                            cmHeight={ state.height }
                            units="м"
                            picture={ getPicture(state.type) }
                            onChange={ this.onChange.bind(this) } />
                    </div>

                    <div className="sign__sidebar">
                        <Options
                            state={ state }
                            types={ types }
                            fonts={ fonts }
                            onChange={ this.onChange.bind(this) } />
                    </div>
                </div>
                <div style={{ marginTop: 20 }}>
                    <div className="sign__order">
                        <div className="sign__orderValue">Итого: <b><span>{ state.price }</span> р.</b></div>
                        <div className="sign__orderTime">Сроки изготовления: 5 &mdash; 7 дней</div>
                        <button className="sign__button sign__button--large" onClick={ this.onSend.bind(this) }>Оформить заказ</button>
                    </div>
                </div>
            </div>
        );
    };

}