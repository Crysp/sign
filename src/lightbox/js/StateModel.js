import Backbone from 'backbone';

let request = null;
class StateModel extends Backbone.Model {

    constructor(attributes) {
        super(attributes);
        this.on('change:type change:width change:height', () => this.fetchPrice());
    };

    defaults() {
        return {
            type: 1,
            width: 1,
            height: 1.5,
            price: 0
        };
    };

    fetchPrice() {
        if (request !== null) request.abort();
        request = Backbone.ajax({
            url: 'http://klukva.red/api/getsvetkorob',
            method: 'get',
            data: {
                type: this.get('type'),
                width: parseFloat(this.get('width')) * 100,
                height: parseFloat(this.get('height')) * 100
            },
            success: (res) => {
                request = null;
                this.set({
                    price: res.price || 0
                })
            }
        });
    };

}

let model = new StateModel();

export default {
    model,
    Class: StateModel
}