import React from 'react';
import ReactDOM from 'react-dom';

import LightBox from './LightBox';

ReactDOM.render(<LightBox />, document.getElementById('lightbox-klukva'));