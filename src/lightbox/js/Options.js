import React from 'react';
import { ChromePicker } from 'react-color';

class ColorPicker extends React.Component {

    state = {
        displayColorPicker: false
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '2'
        };
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        };
        let color = this.props.color;
        return (
            <div className="sign__colorWrapper">
                <button className="sign__colorButton" style={{ backgroundColor: color }} onClick={ this.handleClick }>Pick Color</button>
                { this.state.displayColorPicker ? <div style={ popover }>
                    <div style={ cover } onClick={ this.handleClose } />
                    <ChromePicker color={ color } onChangeComplete={ color => this.props.onChange(this.props.name, color) } />
                </div> : null }
            </div>
        )
    };

}

class SubTypeButton extends React.Component {

    render() {
        let type = this.props.type;
        let typeClasses = ['sign__button sign__button--hollow'];
        if (this.props.active) typeClasses.push('sign__button--active');
        return (
            <button className={ typeClasses.join(' ') } onClick={ ev => this.props.onChange('type', type.index) }>{ type.title }</button>
        );
    };

}

class TypeButton extends React.Component {

    render() {
        let state = this.props.state;
        let type = this.props.type;
        let active = false;
        let typeClasses = ['sign__button sign__button--hollow sign__button--block sign__button--attached'];

        if (state.type === type.index) active = true;
        else if (type.sub.length) {
            type.sub.map(subType => {
                if (subType.index === state.type) active = true;
            });
        }

        if (active) typeClasses.push('sign__button--active');
        return (
            <div className="sign__typeButtonWrapper">
                <button className={ typeClasses.join(' ') } onClick={ ev => this.props.onChange('type', type.sub.length ? type.sub[0].index : type.index) }>{ type.title }</button>
                { active && (
                    <div className="sign__buttonDescription">{ this.props.active.description }</div>
                ) }
                { (type.sub.length > 0 && active) && (
                    <div className="sign__subTypeButtonWrapper">
                        { type.sub.map(subType => <SubTypeButton key={ subType.index } type={ subType } active={ state.type === subType.index } onChange={ this.props.onChange } />) }
                    </div>
                ) }
            </div>
        );
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
    };
    getActiveType() {
        let active = null;
        this.props.types.forEach(type => {
            if (type.index == this.props.state.type) active = type;
            else if (type.sub.length) {
                type.sub.forEach(subType => {
                    if (subType.index == this.props.state.type) active = subType;
                });
            }
        });
        return active;
    };
    render() {
        let state = this.props.state;
        let types = this.props.types;
        return (
            <div className="sign__sidebar">
                <div className="sign__optionTitle">Тип короба</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        { types.map(type => <TypeButton key={ type.index } state={ state } type={ type } active={ this.getActiveType() } onChange={ this.props.onChange } />) }
                    </div>
                </div>
            </div>
        );
    };

}