import React from 'react';
import ReactDOM from 'react-dom';

class Line extends React.Component {

    constructor(props) {
        super(props);

        this.bounds = null;

        this.state = {
            width: 0,
            height: 0,
            dragging: false,
            pos: {
                x: this.props.line.x,
                y: this.props.line.y
            },
            rel: null
        };

        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
    };

    componentDidMount() {
        this.setBounds();
    };

    componentDidUpdate(props, state) {
        this.setBounds();
        if (this.state.dragging && !state.dragging) {
            document.addEventListener('mousemove', this.onMouseMove);
            document.addEventListener('mouseup', this.onMouseUp);
        } else if (!this.state.dragging && state.dragging) {
            document.removeEventListener('mousemove', this.onMouseMove);
            document.removeEventListener('mouseup', this.onMouseUp);
        }
    };

    setBounds() {
        if (this.state.width != this.bounds.clientWidth || this.state.height != this.bounds.clientHeight) {
            this.setState({
                width: this.bounds.clientWidth,
                height: this.bounds.clientHeight
            });
        }
    };

    onMouseDown(ev) {
        // only left mouse button
        if (ev.button !== 0) return;
        var pos = ReactDOM.findDOMNode(this);
        this.props.onMoveSign(this.props.index, ev.pageX - pos.offsetLeft, ev.pageY - pos.offsetTop);
        this.setState({
            dragging: true,
            rel: {
                x: ev.pageX - pos.offsetLeft,
                y: ev.pageY - pos.offsetTop
            }
        });
        //ev.stopPropagation();
        //ev.preventDefault();
    };

    onMouseUp(ev) {
        this.setState({dragging: false});
        //ev.stopPropagation();
        //ev.preventDefault();
    };

    onMouseMove(ev) {
        if (!this.state.dragging) return;
        this.props.onMoveSign(this.props.index, ev.pageX - this.state.rel.x, ev.pageY - this.state.rel.y);
        this.setState({
            pos: {
                x: ev.pageX - this.state.rel.x,
                y: ev.pageY - this.state.rel.y
            }
        });
        ev.stopPropagation();
        ev.preventDefault();
    };

    render() {
        let state = this.props.state;
        let line = this.props.line;
        let htmlText = line.text.replace(/(?:\r\n|\r|\n)/g, '<br>');
        let styles = {
            width: this.state.width + 9,
            height: this.state.height + 9,
            top: this.state.pos.y,
            left: this.state.pos.x,
            color: state.color
        };
        if (htmlText.match(/<br>$/)) htmlText += '<br>';
        return (
            <div
                className={ 'sign__previewAreaWrapper sign__previewAreaWrapper--' + state.font }
                style={ styles }
                onMouseDown={ this.onMouseDown.bind(this) }>
                <textarea
                    className={ 'sign__previewEditArea sign__previewArea sign__previewArea--' + state.font }
                    value={ line.text }
                    onChange={ ev => this.props.onChange('line', this.props.index, ev.target.value) }
                    onMouseDown={ ev => ev.stopPropagation() } />
                <div
                    ref={ bounds => this.bounds = bounds }
                    className={ 'sign__previewEditArea sign__previewAreaBounds sign__previewAreaBounds--' + state.font }
                    dangerouslySetInnerHTML={{ __html: htmlText }}>
                </div>
            </div>
        );
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        return (
            <div className="sign__boxBounds">
                <div className="sign__yAxis">
                    <input type="number" name="height" className="sign__axisValue" value={ state.height } step="1" onChange={ ev => this.props.onChange('input', ev) } />
                </div>
                <div className="sign__xAxis">
                    <input type="number" name="width" className="sign__axisValue" value={ state.width } step="1" onChange={ ev => this.props.onChange('input', ev) } />
                </div>
                <div className="sign__boxWrapper">
                    <div className="sign__box">
                        <div className="sign__boxSide sign__boxSide--top"></div>
                        <div className="sign__boxSide sign__boxSide--front" style={{ backgroundColor: state.backColor }}>
                            <div>
                                { state.lines.map((line, i) => <Line key={ i } index={ i } line={ line } state={ state } onChange={ this.props.onChange } onMoveSign={ this.props.onMoveSign } />) }
                            </div>
                        </div>
                        <div className="sign__boxSide sign__boxSide--bottom"></div>
                    </div>
                </div>
            </div>
        );
    };

}