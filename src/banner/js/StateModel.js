import Backbone from 'backbone';

class Line extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);
    };

    defaults() {
        return {
            text: '',
            x: 0,
            y: 0
        };
    };

}

let request = null;
class StateModel extends Backbone.Model {

    constructor(attributes, options) {
        super(attributes, options);

        this.lines = [
            new Line({
                text: 'Строка 1',
                x: 115,
                y: 20
            }),
            new Line({
                text: 'Строка 2',
                x: 115,
                y: 100
            }),
            new Line({
                text: 'Строка 3',
                x: 115,
                y: 180
            })
        ];

        this.on('change:type change:width change:height', () => this.fetchPrice());
    };

    defaults() {
        return {
            type: 1,
            font: 'agencyb',
            fontSize: 33,
            color: '#333',
            backColor: '#EBECEC',
            picture: null,
            width: 2, // m
            height: 1.5, // m
            price: 0
        };
    };

    fetchPrice() {
        if (request !== null) request.abort();
        request = Backbone.ajax({
            url: 'http://klukva.red/api/getsvetkorob',
            method: 'get',
            data: {
                type: this.get('type'),
                width: parseFloat(this.get('width')) * 100,
                height: parseFloat(this.get('height')) * 100
            },
            success: (res) => {
                request = null;
                this.set({
                    price: res.price || 0
                })
            }
        });
    };

    toJSON() {
        let json = Backbone.Model.prototype.toJSON.call(this);
        json.lines = this.lines.map(line => line.toJSON());
        return json;
    };

}

let model = new StateModel();

export default {
    model
}