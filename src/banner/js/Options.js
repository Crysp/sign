import React from 'react';
import { ChromePicker } from 'react-color';
import { Field } from '../../components/Options';

class ColorPicker extends React.Component {

    state = {
        displayColorPicker: false
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '300'
        };
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        };
        let color = this.props.color;
        return (
            <div className="sign__colorWrapper">
                <button className="sign__colorButton" style={{ backgroundColor: color }} onClick={ this.handleClick }>Pick Color</button>
                { this.state.displayColorPicker ? <div style={ popover }>
                    <div style={ cover } onClick={ this.handleClose } />
                    <ChromePicker color={ color } onChangeComplete={ color => this.props.onChange(this.props.name, color) } />
                </div> : null }
            </div>
        )
    };

}

class TypeButton extends React.Component {

    render() {
        let state = this.props.state;
        let type = this.props.type;
        let active = false;
        let typeClasses = ['sign__button sign__button--hollow sign__button--block sign__button--attached'];

        if (state.type === type.uid) active = true;

        if (active) typeClasses.push('sign__button--active');
        return (
            <div className="sign__typeButtonWrapper">
                <button className={ typeClasses.join(' ') } onClick={ ev => this.props.onChange('type', type.uid) }>{ type.title }</button>
                { active && type.description && (
                    <div className="sign__buttonDescription">{ type.description }</div>
                ) }
            </div>
        );
    };

}

export default class extends React.Component {

    constructor(props) {
        super(props);
    };

    render() {
        let state = this.props.state;
        let fonts = this.props.fonts;
        let types = this.props.types;
        return (
            <div className="sign__sidebar">
                <div className="sign__optionTitle">Тип баннера</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        { types.map(type => <TypeButton key={ type.uid } state={ state } type={ type } onChange={ this.props.onChange } />) }
                    </div>
                </div>
                <div className="sign__optionTitle">Текст</div>
                <div className="sign__optionContent">
                    <div className="sign__row">
                        <label htmlFor="font" className="sign__label">Тип шрифта</label>
                        <select
                            name="font"
                            id="font"
                            className="sign__select"
                            value={ state.font }
                            onChange={ ev => this.props.onChange('select', ev) }>
                            { fonts.map(({ tag, title }) => <option key={ tag } value={ tag }>{ title }</option>) }
                        </select>
                    </div>
                    <div className="sign__row">
                        <label htmlFor="color" className="sign__label">Цвет</label>
                        <ColorPicker name="color" color={ state.color } onChange={ this.props.onChange } />
                    </div>
                </div>
                <div className="sign__optionTitle">Фон</div>
                <div className="sign__optionContent">
                    <Field
                        label="Цвет"
                        type="color"
                        defaultValue={ state.backColor }
                        onChange={ value => this.props.onChange('backColor', value) } />
                    <p style={{ textAlign: 'center', margin: '10px 0 10px 0' }}>или</p>
                    <Field
                        label="Картинка"
                        type="file"
                        defaultValue={ this.props.selectedPicture }
                        onChange={ value => this.props.onChange('picture', value) } />
                </div>
            </div>
        );
    };

}