import _ from 'underscore';
import Backbone from 'backbone';
import React from 'react';
import Cookies from '../../lib/cookie';

import fonts from '../../data/fonts';
import State from './StateModel';
import { QueryModel } from '../../components/QueryModel';

import Options from './Options';
import BoxPreview from '../../components/BoxPreview';
import EditableText from '../../components/EditableText';

// const types = [
//     { index: 1, title: 'Премиум', description: 'Короб украшен псевдообъемными буквами и инкрустирован акрилом', sub: [] },
//     { index: 2, title: 'Фигурный', description: 'Короб украшен псевдообъемными буквами и инкрустирован акрилом', sub: [] },
//     { index: 3, title: 'Стандарт', description: 'Короб украшен псевдообъемными буквами и инкрустирован акрилом', sub: [
//         { index: 4, title: '1' },
//         { index: 5, title: '2' },
//         { index: 6, title: '3' },
//         { index: 7, title: '4' }
//     ]},
//     { index: 8, title: 'Эконом', description: 'Короб украшен псевдообъемными буквами и инкрустирован акрилом', sub: [] }
// ];

const materials = [
    { uid: 'pvh', title: 'ПВХ', types: [
        { uid: 'fullColored', title: 'Полноцветная печать', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' },
        { uid: 'uf', title: 'УФ-печать', description: 'Описание' },
    ], depths: [3, 5] },
    { uid: 'acryl', title: 'Акрил', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' },
        { uid: 'uf', title: 'УФ-печать', description: 'Описание' },
    ], depths: [3, 5, 6, 8, 10] },
    { uid: 'plastic', title: 'Двухслойный пластик', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' }
    ], depths: [1.5, 2.5, 3] },
    { uid: 'panel', title: 'Алюминиевые композитные панели', types: [
        { uid: 'fullColored', title: 'Полноцветная печать', description: 'Описание' },
        { uid: 'plotter', title: 'Плоттерная резка', description: 'Описание' }
    ], depths: [3] },
    { uid: 'plywood', title: 'Фанера', types: [
        { uid: 'etching', title: 'Гравировка', description: 'Описание' }
    ], depths: [4, 6] }
];

const types = [
    { uid: 1, title: 'Интерьерный' },
    { uid: 2, title: 'Уличный' },
    { uid: 3, title: 'Пресс-волл' },
];

const boxWidth = 450;
const boxHeight = 260;
const fontHeightPx = 24;

export default class extends React.Component {
    static defaultProps = {
        query: new QueryModel()
    };
    constructor(props) {
        super(props);
        State.model.set(this.props.query.attributes);
        this.state = {
            model: State.model,
            defaultFontHeight: Number(State.model.get('fontSize'))
        };

        this.vent = _.extend({}, Backbone.Events);

        this.vent.listenTo(this.state.model, 'change', model => this.setState({ model }));
        this.state.model.lines.map(line => this.vent.listenTo(line, 'change', () => this.forceUpdate()));
    };

    componentDidMount() {
        this.state.model.fetchPrice();
    };

    componentWillUnmount() {
        this.vent.off();
    };

    onChangeLine(index, value) {
        this.state.model.lines[index].set({ text: value });
    };

    onChange(type, i, value) {
        if (typeof value == 'undefined') value = i;
        switch(type) {
            case 'type':
                this.state.model.set({ [type]: value });
                break;
            case 'select':
            case 'input':
                if (value.target.name == 'material') {
                    let material = materials.filter(material => material.uid == value.target.value)[0];
                    this.state.model.set({ type: material.types[0].uid });
                }
                this.state.model.set({ [value.target.name]: value.target.value });
                break;
            case 'color':
            case 'backColor':
                this.state.model.set({ [type]: value.hex });
                break;
            case 'picture':
                let image = new Image();
                image.src = value;
                image.onload = () => {
                    this.state.model.set({
                        // width: this.pxToCm(image.width),
                        // height: this.pxToCm(image.height),
                        picture: value
                    });
                };
                break;
            default:
                this.state.model.set({ [type]: i });
        }
    };
    pxToCm(px) {
        return (this.state.defaultFontHeight / fontHeightPx) * px;
    };

    onMoveSign(index, x, y) {
        this.state.model.lines[index].set({ x, y });
    };

    onSend(e) {
        e.preventDefault();
        Cookies.set('klukva-banner', JSON.stringify(this.state.model.toJSON()));
    };

    render() {
        let state = this.state.model.toJSON();
        return (
            <div>
                <div className="sign__calc">
                    <h2>Онлайн конструктор банеров</h2>

                    <div className="sign__container">
                        <BoxPreview
                            width={ boxWidth }
                            height={ boxHeight }
                            lines={state.lines}
                            cmWidth={ state.width }
                            cmHeight={ state.height }
                            cmFontHeight={ fontHeightPx }
                            units="м"
                            volume={false}
                            defaultFontHeight={ this.state.defaultFontHeight }
                            background={ state.backColor }
                            picture={ state.picture }
                            state={ state }
                            onChange={ this.onChange.bind(this) }
                            onChangeLine={ this.onChangeLine.bind(this) }
                            onMoveSign={ this.onMoveSign.bind(this) }>
                        </BoxPreview>
                    </div>

                    <Options
                        state={ state }
                        types={ types }
                        fonts={ fonts }
                        onChange={ this.onChange.bind(this) } />
                </div>
                <div style={{ marginTop: 20 }}>
                    <div className="sign__order">
                        <div className="sign__orderValue">Итого: <b><span>{ state.price }</span> р.</b></div>
                        <div className="sign__orderTime">Сроки изготовления: 2 &mdash; 3 дней</div>
                        <button className="sign__button sign__button--large" onClick={this.onSend.bind(this)}>Оформить заказ</button>
                    </div>
                </div>
            </div>
        );
    };

}